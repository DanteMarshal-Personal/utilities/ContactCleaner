﻿using vCardLib.Models;
using System.Collections.Generic;
using System;

namespace ContactCleaner
{
	internal static class Printer
	{
		public static string PrintVCard(vCard vCard, int linePadding = 8)
		{
			var lines = vCard.ToString().Replace("\r", "").Split("\n");
			for (var i = 0; i < lines.Length; i++)
				lines[i] = lines[i].PadRight(linePadding);
			return string.Join(" | ", lines);
		}
		public static string MultiPrintVCards(IList<vCard> vCards, bool printIndices = false, string cardSeparator = "\n", string splitSeparator = " | ", string indexFormat = "vCard  0")
		{
			var vLen = vCards.Count;
			var lines = new string[vLen][];
			var maxLineSplits = 0;
			for (var i = 0; i < vLen; i++)
			{
				lines[i] = vCards[i].ToString().Replace("\r", "").Split("\n");
				maxLineSplits = Math.Max(maxLineSplits, lines[i].Length);
			}
			var lineLens = new int[maxLineSplits];
			for (var i = 0; i < vLen; i++)
			{
				var lineSplits = lines[i].Length;
				for (var j = 0; j < lineSplits; j++)
					lineLens[j] = Math.Max(lineLens[j], lines[i][j].Length);
			}
			var ret = new string[vLen];
			for (var i = 0; i < vLen; i++)
			{
				var lineSplits = lines[i].Length;
				for (var j = 0; j < lineSplits; j++)
					lines[i][j] = lines[i][j].PadRight(lineLens[j]);
				ret[i] = string.Join(splitSeparator, lines[i]);
				if (printIndices)
					ret[i] = $"{(i + 1).ToString(indexFormat)} : {ret[i]}";
			}
			return string.Join(cardSeparator, ret);
		}
		public static string MultiPrintVCards(IList<Tuple<int, vCard>> vCards, bool printIndices = false, string cardSeparator = "\n", string splitSeparator = " | ", string indexFormat = "vCard  0")
		{
			var vLen = vCards.Count;
			var lines = new string[vLen][];
			var maxLineSplits = 0;
			for (var i = 0; i < vLen; i++)
			{
				lines[i] = vCards[i].Item2.ToString().Replace("\r", "").Split("\n");
				maxLineSplits = Math.Max(maxLineSplits, lines[i].Length);
			}
			var lineLens = new int[maxLineSplits];
			for (var i = 0; i < vLen; i++)
			{
				var lineSplits = lines[i].Length;
				for (var j = 0; j < lineSplits; j++)
					lineLens[j] = Math.Max(lineLens[j], lines[i][j].Length);
			}
			var ret = new string[vLen];
			for (var i = 0; i < vLen; i++)
			{
				var lineSplits = lines[i].Length;
				for (var j = 0; j < lineSplits; j++)
					lines[i][j] = lines[i][j].PadRight(lineLens[j]);
				ret[i] = string.Join(splitSeparator, lines[i]);
				if (printIndices)
					ret[i] = $"{(vCards[i].Item1 + 1).ToString(indexFormat)} : {ret[i]}";
			}
			return string.Join(cardSeparator, ret);
		}
	}
}