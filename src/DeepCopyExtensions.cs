﻿using System;
using System.Collections.Generic;
using vCardLib.Models;

namespace ContactCleaner
{
	internal static class DeepCopyExtensions
	{
		public static List<vCard> DeepCopy(this IList<vCard> l)
		{
			var ret = new List<vCard>();
			foreach (var e in l)
				ret.Add(e.DeepCopy());
			return ret;
		}
		public static List<Address> DeepCopy(this IList<Address> l)
		{
			var ret = new List<Address>();
			foreach (var e in l)
				ret.Add(e.DeepCopy());
			return ret;
		}
		public static List<KeyValuePair<string, string>> DeepCopy(this IList<KeyValuePair<string, string>> l)
		{
			var ret = new List<KeyValuePair<string, string>>();
			foreach (var e in l)
				ret.Add(new KeyValuePair<string, string>(e.Key, e.Value));
			return ret;
		}
		public static List<EmailAddress> DeepCopy(this IList<EmailAddress> l)
		{
			var ret = new List<EmailAddress>();
			foreach (var e in l)
				ret.Add(e.DeepCopy());
			return ret;
		}
		public static List<Expertise> DeepCopy(this IList<Expertise> l)
		{
			var ret = new List<Expertise>();
			foreach (var e in l)
				ret.Add(e.DeepCopy());
			return ret;
		}
		public static List<Hobby> DeepCopy(this IList<Hobby> l)
		{
			var ret = new List<Hobby>();
			foreach (var e in l)
				ret.Add(e.DeepCopy());
			return ret;
		}
		public static List<Interest> DeepCopy(this IList<Interest> l)
		{
			var ret = new List<Interest>();
			foreach (var e in l)
				ret.Add(e.DeepCopy());
			return ret;
		}
		public static List<PhoneNumber> DeepCopy(this IList<PhoneNumber> l)
		{
			var ret = new List<PhoneNumber>();
			foreach (var e in l)
				ret.Add(e.DeepCopy());
			return ret;
		}
		public static List<Photo> DeepCopy(this IList<Photo> l)
		{
			var ret = new List<Photo>();
			foreach (var e in l)
				ret.Add(e.DeepCopy());
			return ret;
		}
		public static vCard DeepCopy(this vCard c)
		{
			return new vCard()
			{
				Addresses = c.Addresses.DeepCopy(),
				BirthDay = c.BirthDay,
				BirthPlace = c.BirthPlace,
				CustomFields = c.CustomFields.DeepCopy(),
				DeathPlace = c.DeathPlace,
				EmailAddresses = c.EmailAddresses.DeepCopy(),
				Expertises = c.Expertises.DeepCopy(),
				FamilyName = c.FamilyName,
				FormattedName = c.FormattedName,
				Gender = c.Gender,
				Geo = c.Geo?.DeepCopy(),
				GivenName = c.GivenName,
				Hobbies = c.Hobbies.DeepCopy(),
				Interests = c.Interests.DeepCopy(),
				Kind = c.Kind,
				Language = c.Language,
				MiddleName = c.MiddleName,
				NickName = c.NickName,
				Note = c.Note,
				Organization = c.Organization,
				PhoneNumbers = c.PhoneNumbers.DeepCopy(),
				Pictures = c.Pictures.DeepCopy(),
				Prefix = c.Prefix,
				Revision = c.Revision,
				Suffix = c.Suffix,
				TimeZone = c.TimeZone,
				Title = c.Title,
				Url = c.Url,
				Version = c.Version,
			};
		}
		public static Address DeepCopy(this Address v)
		{
			return new Address()
			{
				Type = v.Type,
				Location = v.Location,
			};
		}
		public static EmailAddress DeepCopy(this EmailAddress v)
		{
			return new EmailAddress()
			{
				Type = v.Type,
				Email = v.Email,
			};
		}
		public static Expertise DeepCopy(this Expertise v)
		{
			return new Expertise()
			{
				Area = v.Area,
				Level = v.Level,
			};
		}
		public static Geo DeepCopy(this Geo v)
		{
			return new Geo()
			{
				Latitude = v.Latitude,
				Longitude = v.Longitude,
			};
		}
		public static Hobby DeepCopy(this Hobby v)
		{
			return new Hobby()
			{
				Activity = v.Activity,
				Level = v.Level,
			};
		}
		public static Interest DeepCopy(this Interest v)
		{
			return new Interest()
			{
				Activity = v.Activity,
				Level = v.Level,
			};
		}
		public static PhoneNumber DeepCopy(this PhoneNumber v)
		{
			return new PhoneNumber()
			{
				Type = v.Type,
				Number = v.Number,
			};
		}
		public static Photo DeepCopy(this Photo v)
		{
			return new Photo()
			{
				Type = v.Type,
				Encoding = v.Encoding,
				PhotoURL = v.PhotoURL,
				Picture = new List<byte>(v.Picture).ToArray()
			};
		}
	}
}
