﻿using System;
using vCardLib.Enums;
using vCardLib.Models;
using System.Collections.Generic;
using System.IO;

namespace ContactCleaner
{
	internal static class Program
	{
		private static string defaultCountryCode;
		private static void Main(string[] args)
		{
			if (args.Length != 3)
			{
				Console.Error.WriteLine("Invalid Arguments. Usage :\n\tContactsCleaner.exe <Input.vcf> <Output.vcf> <Default Country Code>");
				return;
			}
			defaultCountryCode = args[2];
			while (true)
			{
				Run(args[0], args[1]);
				Console.WriteLine("Done. Press 'R' key to repeat the process on result (for finding even more duplicates); Or any other key to exit.");
				if (Console.ReadKey(true).Key != ConsoleKey.R)
					return;
				args[0] = args[1];
			}
		}
		private static void Run(string src, string dst)
		{
			var vCards = vCardLib.Deserializers.Deserializer.FromFile(src);
			Console.WriteLine($"- Unifying formats : Total {vCards.Count} contacts");
			for (var i = 0; i < vCards.Count; i++)
				vCards[i] = UnifyNumberFormats(vCards[i]);
			Console.WriteLine("- Finding potential Duplicates");
			var duplicates = FindPotentialDuplicates(vCards);
			Console.WriteLine($"Found {duplicates.Count} duplicate(s)");
			var dp = new DuplicateProcessor(vCards, duplicates);
			if (duplicates.Count > 0)
				dp.Run();
			Console.WriteLine($"Saving results to '{dst}'");
			var final = string.Join('\n', dp.Result);
			File.WriteAllText(dst, final);
		}
		private static vCard UnifyNumberFormats(vCard vCard)
		{
			static string UnifyNumberFormat(string number)
			{
				number = number.Replace(" ", null, StringComparison.InvariantCultureIgnoreCase);
				if (number.StartsWith("00"))
					number = "+" + number.Remove(0, 2);
				else if (number.StartsWith("0"))
					number = defaultCountryCode + number.Remove(0, 2);
				return number;
			}
			var pns = vCard.PhoneNumbers;
			var ns = new Dictionary<string, PhoneNumberType>();
			for (var i = 0; i < pns.Count; i++)
			{
				var n = UnifyNumberFormat(pns[i].Number);
				if (!ns.ContainsKey(n))
					ns.Add(n, pns[i].Type);
			}
			vCard.PhoneNumbers.Clear();
			foreach (var n in ns)
				vCard.PhoneNumbers.Add(new PhoneNumber() { Number = n.Key, Type = n.Value });
			return vCard;
		}
		private static IList<IList<int>> FindPotentialDuplicates(List<vCard> vCards, float stringSimilarityThreshold = 0.75f)
		{
			static float GetNameSimilarity(vCard vI, vCard vJ)
			{
				static int Levenshtein(string s, string t)
				{
					if (string.IsNullOrEmpty(s))
					{
						if (string.IsNullOrEmpty(t))
							return 0;
						return t.Length;
					}
					if (string.IsNullOrEmpty(t))
						return s.Length;
					int n = s.Length;
					int m = t.Length;
					int[,] d = new int[n + 1, m + 1];
					// initialize the top and right of the table to 0, 1, 2, ...
					for (int i = 0; i <= n; d[i, 0] = i++) ;
					for (int j = 1; j <= m; d[0, j] = j++) ;
					for (int i = 1; i <= n; i++)
					{
						for (int j = 1; j <= m; j++)
						{
							int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;
							int min1 = d[i - 1, j] + 1;
							int min2 = d[i, j - 1] + 1;
							int min3 = d[i - 1, j - 1] + cost;
							d[i, j] = Math.Min(Math.Min(min1, min2), min3);
						}
					}
					return d[n, m];
				}
				int simMax = 0, simLen;
				float simTemp = float.PositiveInfinity, simMost = float.PositiveInfinity;
				// First Name
				simLen = Math.Min(vI.GivenName?.Length ?? 0, vJ.GivenName?.Length ?? 0);
				if (simLen > 0)
				{
					simTemp = (float)Levenshtein(vI.GivenName, vJ.GivenName) / (float)Math.Max(vI.GivenName.Length, vJ.GivenName.Length);
					if (simTemp < simMost)
						simMost = simTemp;
				}
				// Middle Name
				simLen = Math.Min(vI.MiddleName?.Length ?? 0, vJ.MiddleName?.Length ?? 0);
				if (simLen > 0)
				{
					simTemp = (float)Levenshtein(vI.MiddleName, vJ.MiddleName) / (float)Math.Max(vI.MiddleName.Length, vJ.MiddleName.Length);
					if (simTemp < simMost)
						simMost = simTemp;
				}
				// Last Name
				simLen = Math.Min(vI.FamilyName?.Length ?? 0, vJ.FamilyName?.Length ?? 0);
				if (simLen > 0)
				{
					simTemp = (float)Levenshtein(vI.FamilyName, vJ.FamilyName) / (float)Math.Max(vI.FamilyName.Length, vJ.FamilyName.Length);
					if (simTemp < simMost)
						simMost = simTemp;
				}
				// Nick Name
				simLen = Math.Min(vI.NickName?.Length ?? 0, vJ.NickName?.Length ?? 0);
				if (simLen > 0)
				{
					simTemp = (float)Levenshtein(vI.NickName, vJ.NickName) / (float)Math.Max(vI.NickName.Length, vJ.NickName.Length);
					if (simTemp < simMost)
						simMost = simTemp;
				}
				// Formatted Name
				simLen = Math.Min(vI.FormattedName?.Length ?? 0, vJ.FormattedName?.Length ?? 0);
				if (simLen > 0)
				{
					simTemp = (float)Levenshtein(vI.FormattedName, vJ.FormattedName) / (float)Math.Max(vI.FormattedName.Length, vJ.FormattedName.Length);
					if (simTemp < simMost)
						simMost = simTemp;
				}
				// Score
				if (simMax == 0)
					return 0f;
				return 1f - ((float)simMost) / ((float)simMax);
			}
			static bool CheckSimilarDetails(vCard vI, vCard vJ)
			{
				var sI = new HashSet<string>();
				var sJ = new HashSet<string>();
				// Check Phone Numbers
				{
					var nI = vI.PhoneNumbers;
					var nJ = vJ.PhoneNumbers;
					foreach (var ni in nI)
						if (!string.IsNullOrWhiteSpace(ni.Number))
							sI.Add(ni.Number);
					foreach (var nj in nJ)
						if (!string.IsNullOrWhiteSpace(nj.Number))
							sJ.Add(nj.Number);
					if (sI.Overlaps(sJ))
						return true;
					sI.Clear(); sJ.Clear();
				}
				// Check Email Addresses
				{
					var eI = vI.EmailAddresses;
					var eJ = vJ.EmailAddresses;
					foreach (var ni in eI)
						if (!string.IsNullOrWhiteSpace(ni.Email))
							sI.Add(ni.Email);
					foreach (var nj in eJ)
						if (!string.IsNullOrWhiteSpace(nj.Email))
							sJ.Add(nj.Email);
					if (sI.Overlaps(sJ))
						return true;
					sI.Clear(); sJ.Clear();
				}
				// Check Physical Addresses
				{
					var eI = vI.Addresses;
					var eJ = vJ.Addresses;
					foreach (var ni in eI)
						if (!string.IsNullOrWhiteSpace(ni.Location))
							sI.Add(ni.Location);
					foreach (var nj in eJ)
						if (!string.IsNullOrWhiteSpace(nj.Location))
							sJ.Add(nj.Location);
					if (sI.Overlaps(sJ))
						return true;
					sI.Clear(); sJ.Clear();
				}
				return false;
			}
			var ret = new List<IList<int>>();
			var vCount = vCards.Count;
			var doneSoSkip = new HashSet<int>();
			for (var i = 0; i < vCount; i++)
			{
				if (doneSoSkip.Contains(i))
					continue;
				var dupes = new List<int>(1) { i };
				var vI = vCards[i];
				for (var j = i + 1; j < vCount; j++)
				{
					if (doneSoSkip.Contains(j))
						continue;
					var vJ = vCards[j];
					var haveSimilarities = GetNameSimilarity(vI, vJ) > stringSimilarityThreshold;
					haveSimilarities = haveSimilarities || CheckSimilarDetails(vI, vJ);
					if (haveSimilarities)
						dupes.Add(j);
				}
				if (dupes.Count > 1)
				{
					ret.Add(dupes);
					doneSoSkip.Add(i);
					doneSoSkip.UnionWith(dupes);
				}
			}
			return ret;
		}
	}
}
