﻿using vCardLib.Models;
using System.Collections.Generic;
using System;
namespace ContactCleaner
{
	internal class DuplicateProcessor
	{
		private readonly IList<vCard> src, dst;
		private readonly HashSet<int> toBeRemoved;
		private readonly IList<IList<int>> duplicates;
		internal IList<vCard> Result
		{
			get
			{
				var ret = this.dst.DeepCopy();
				var idx = new List<int>(toBeRemoved);
				idx.Sort();
				for (var i = idx.Count - 1; i >= 0; i--)
					ret.RemoveAt(idx[i]);
				return ret;
			}
		}
		internal DuplicateProcessor(IList<vCard> vCards, IList<IList<int>> duplicates)
		{
			this.src = vCards;
			this.duplicates = duplicates;
			this.toBeRemoved = new HashSet<int>();
			this.dst = src.DeepCopy();
		}
		private bool isRan = false;
		internal void Run()
		{
			if (isRan)
				throw new InvalidOperationException("DuplicateProcessor already ran once. Create a new one !");
			isRan = true;
			var dupeCount = duplicates.Count;
			for (var dIdx = 0; dIdx < dupeCount; dIdx++)
			{
				var dupes = new List<vCard>(duplicates[dIdx].Count);
				foreach (var i in duplicates[dIdx])
					dupes.Add(src[i]);
				bool isProcessing = true;
				while (isProcessing)
				{
					char action = 's';
					if (dupes.Count > 1)
					{
						Console.WriteLine();
						Console.WriteLine($"- Processing Duplicate : {dIdx + 1} / {dupeCount}");
						action = this.SelectAction(dupes);
					}
					else Console.WriteLine("Last Remaining Card : " + Printer.MultiPrintVCards(dupes));
					switch (action)
					{
						case 's':
							Console.WriteLine("Saving ...\n");
							isProcessing = false;
							break;
						case 'm':
							Console.WriteLine("Merging ...\n");
							this.RunMerge(duplicates[dIdx], dupes);
							break;
						case 'r':
							Console.WriteLine("Removing ...\n");
							this.RunRemove(duplicates[dIdx], dupes);
							break;
					}
				}
			}
		}
		private char SelectAction(IList<vCard> dupes)
		{
			while (true)
			{
				Console.WriteLine(Printer.MultiPrintVCards(dupes, true));
				Console.WriteLine("Choose what to do :");
				Console.WriteLine("s: Save");
				Console.WriteLine("m: Merge");
				Console.WriteLine("r: Remove");
				var option = Console.ReadKey(true).KeyChar.ToString().ToLower()[0];
				switch (option)
				{
					case 's':
					case 'm':
					case 'r':
						return option;
				}
			}
		}
		private void RunMerge(IList<int> indices, List<vCard> dupes)
		{
			int idx;
			while (true)
			{
				Printer.MultiPrintVCards(dupes);
				Console.Write("Choose index to keep as defaults (0 to go back) : ");
				if (!int.TryParse(Console.ReadLine(), out idx))
					continue;
				if (idx == 0)
					return;
				if (idx < 1 || idx > dupes.Count)
					continue;
				var refCard = dupes[idx - 1];
				for (var i = indices.Count - 1; i >= 0; i--)
				{
					if (i == (idx - 1))
						continue;
					refCard.MergeWith(dupes[i]);
					dupes.RemoveAt(i);
					this.toBeRemoved.Add(indices[i]);
					indices.RemoveAt(i);
				}
				return;
			}
		}
		private void RunRemove(IList<int> indices, List<vCard> dupes)
		{
			int idx;
			while (true)
			{
				if (dupes.Count == 0)
				{
					Console.WriteLine("You removed everything !");
					return;
				}
				Printer.MultiPrintVCards(dupes);
				Console.Write("Choose index to remove (0 to go back) : ");
				if (!int.TryParse(Console.ReadLine(), out idx))
					continue;
				if (idx == 0)
					return;
				if (idx < 1 || idx > dupes.Count)
					continue;
				dupes.RemoveAt(idx - 1);
				this.toBeRemoved.Add(indices[idx - 1]);
				indices.RemoveAt(idx - 1);
				return;
			}
		}
	}
}
