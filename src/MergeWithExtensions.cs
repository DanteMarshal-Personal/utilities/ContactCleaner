﻿using System.Collections.Generic;
using vCardLib.Enums;
using vCardLib.Models;

namespace ContactCleaner
{
	internal static class MergeWithExtensions
	{
		public static void MergeWith(this IList<vCard> l, IList<vCard> v)
		{
			foreach (var e in v)
				l.AddUnique(e);
		}
		public static void MergeWith(this IList<Address> l, IList<Address> v)
		{
			foreach (var e in v)
				l.AddUnique(e);
		}
		public static void MergeWith(this IList<KeyValuePair<string, string>> l, IList<KeyValuePair<string, string>> v)
		{
			foreach (var e in v)
				l.AddUnique(e);
		}
		public static void MergeWith(this IList<EmailAddress> l, IList<EmailAddress> v)
		{
			foreach (var e in v)
				l.AddUnique(e);
		}
		public static void MergeWith(this IList<Expertise> l, IList<Expertise> v)
		{
			foreach (var e in v)
				l.AddUnique(e);
		}
		public static void MergeWith(this IList<Hobby> l, IList<Hobby> v)
		{
			foreach (var e in v)
				l.AddUnique(e);
		}
		public static void MergeWith(this IList<Interest> l, IList<Interest> v)
		{
			foreach (var e in v)
				l.AddUnique(e);
		}
		public static void MergeWith(this IList<PhoneNumber> l, IList<PhoneNumber> v)
		{
			foreach (var e in v)
				l.AddUnique(e);
		}
		public static void MergeWith(this IList<Photo> l, IList<Photo> v)
		{
			foreach (var e in v)
				l.AddUnique(e);
		}
		public static void MergeWith(this vCard c, vCard v)
		{
			c.Addresses.MergeWith(v.Addresses);
			if (c.BirthDay == null)
				c.BirthDay = v.BirthDay;
			if (string.IsNullOrWhiteSpace(c.BirthPlace))
				c.BirthPlace = v.BirthPlace;
			c.CustomFields.MergeWith(v.CustomFields);
			if (string.IsNullOrWhiteSpace(c.DeathPlace))
				c.DeathPlace = v.DeathPlace;
			c.EmailAddresses.MergeWith(v.EmailAddresses);
			c.Expertises.MergeWith(v.Expertises);
			if (string.IsNullOrWhiteSpace(c.FamilyName))
				c.FamilyName = v.FamilyName;
			if (string.IsNullOrWhiteSpace(c.FormattedName))
				c.FormattedName = v.FormattedName;
			if (c.Gender == GenderType.None)
				c.Gender = v.Gender;
			if (c.Geo == null)
				c.Geo = v.Geo;
			if (string.IsNullOrWhiteSpace(c.GivenName))
				c.GivenName = v.GivenName;
			c.Hobbies.MergeWith(v.Hobbies);
			c.Interests.MergeWith(v.Interests);
			// c.Kind = v.Kind;
			if (string.IsNullOrWhiteSpace(c.Language))
				c.Language = v.Language;
			if (string.IsNullOrWhiteSpace(c.MiddleName))
				c.MiddleName = v.MiddleName;
			if (string.IsNullOrWhiteSpace(c.NickName))
				c.NickName = v.NickName;
			if (string.IsNullOrWhiteSpace(c.Note))
				c.Note = v.Note;
			if (string.IsNullOrWhiteSpace(c.Organization))
				c.Organization = v.Organization;
			c.PhoneNumbers.MergeWith(v.PhoneNumbers);
			c.Pictures.MergeWith(v.Pictures);
			if (string.IsNullOrWhiteSpace(c.Prefix))
				c.Prefix = v.Prefix;
			if (c.Revision == null)
				c.Revision = v.Revision;
			if (string.IsNullOrWhiteSpace(c.Suffix))
				c.Suffix = v.Suffix;
			if (string.IsNullOrWhiteSpace(c.TimeZone))
				c.TimeZone = v.TimeZone;
			if (string.IsNullOrWhiteSpace(c.Title))
				c.Title = v.Title;
			if (string.IsNullOrWhiteSpace(c.Url))
				c.Url = v.Url;
			if ((int)v.Version > (int)c.Version)
				c.Version = v.Version;
		}
		public static void AddUnique(this IList<vCard> l, vCard v)
		{
			foreach (var e in l)
				if($"{e}" == $"{v}")
					return;
			l.Add(v);
		}
		public static void AddUnique(this IList<Address> l, Address v)
		{
			foreach (var e in l)
				if(e.Location == v.Location)
					return;
			l.Add(v);
		}
		public static void AddUnique(this IList<KeyValuePair<string, string>> l, KeyValuePair<string, string> v)
		{
			foreach (var e in l)
				if(e.Key == v.Key && e.Value == v.Value)
					return;
			l.Add(v);
		}
		public static void AddUnique(this IList<EmailAddress> l, EmailAddress v)
		{
			foreach (var e in l)
				if(e.Email == v.Email)
					return;
			l.Add(v);
		}
		public static void AddUnique(this IList<Expertise> l, Expertise v)
		{
			foreach (var e in l)
				if(e.Area == v.Area)
					return;
			l.Add(v);
		}
		public static void AddUnique(this IList<Hobby> l, Hobby v)
		{
			foreach (var e in l)
				if(e.Activity == v.Activity)
					return;
			l.Add(v);
		}
		public static void AddUnique(this IList<Interest> l, Interest v)
		{
			foreach (var e in l)
				if(e.Activity == v.Activity)
					return;
			l.Add(v);
		}
		public static void AddUnique(this IList<PhoneNumber> l, PhoneNumber v)
		{
			foreach (var e in l)
				if(e.Number == v.Number)
					return;
			l.Add(v);
		}
		public static void AddUnique(this IList<Photo> l, Photo v)
		{
			foreach (var e in l)
				if(e.Type == v.Type && (
					(e.Encoding == v.Encoding && e.ToBase64String() == v.ToBase64String()) || (e.PhotoURL == v.PhotoURL)
				)) return;
			l.Add(v);
		}
	}
}
